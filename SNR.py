import numpy as np

def SNR(traces, labels):
    # Get the number of traces and number of samples per trace
    Nt, Ns = (len(traces), len(traces[0]))
    classes = np.unique(labels)
    Nc = len(classes)
    # Number of observed traces in each intermediate value
    ns = np.zeros((Nc),dtype=np.uint32)
    # Sum for each class
    sum1 = np.zeros((Nc,Ns),dtype=np.float64)
    # Sum of squared traces for each class
    sum2 = np.zeros((Nc,Ns),dtype=np.float64)
    # Mean of each class
    means = np.zeros((Nc,Ns),dtype=np.float64)
    # Variance in each class
    var = np.zeros((Nc,Ns),dtype=np.float64)
    # SNR on each class
    snr = np.zeros((Ns),dtype=np.float64)

    # Compute sum and squared sum for all traces given labels
    for i in range(Nt):
        c_i = np.where(classes==labels[i])
        ns[c_i] += 1
        t = traces[i,:]
        sum1[c_i,:] += t
        sum2[c_i,:] += t*t.astype(np.float64)

    # Compute mean and variance on every class
    for c in range(Nc):
        means[c,:] = (sum1[c,:] / ns[c]) # standard mean
        var[c,:] = (sum2[c,:]/ ns[c]) - (means[c,:]**2) # as Var[X] = E[X**2] - E[X]**2
    # Compute SNR for the given intermediate value as: E[X]/Var[X]
    snr = np.var(means,axis=0)/np.mean(var,axis=0)
    return snr

if __name__ == "__main__":
    (Nt,Ns) = 500,30 # Nt: number of traces in traceset, Ns: number of sample in a single trace
    traces = np.array([np.random.normal(0,1,(Nt,Ns)), np.random.normal(1,1,(Nt,Ns))], dtype=float).reshape(int(2*Nt),Ns)
    labels = np.array([np.zeros(Nt), np.ones(Nt)], dtype=int).reshape(-1)
    Nc = int(np.max(labels)+1) # Nc: number of classes for the possible intermediate values
    snr = SNR(traces, labels, int(2*Nt), Ns, Nc)
    print(snr)
