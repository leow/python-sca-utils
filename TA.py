## Template attack code sample from Léo.
## Handle with care.

import sys,glob, os
import trsfile
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm
import argparse

def diffOfMeansPOI(train_data, train_targets, test_data, npoi, n_class=2):
    """
    Point of interest selection based on the difference of means.
    """
    assert train_data.shape[1]==test_data.shape[1]
    s_length = train_data.shape[1]
    n_training_sample = train_data.shape[0]
    n_testing_sample = test_data.shape[0]
    # assign traces to clusters based on lables
    catagory_for_traces = [[] for _ in range(n_class)]
    for i in tqdm(range(train_data.shape[0]),desc="Sorting traces per operation"):
        catagory_for_traces[train_targets[i]].append(train_data[i])
    # compute the mean each cluster, if the cluster is empty, use the mean of the whole dataset
    mean_op = np.empty(shape=(n_class, s_length))
    for i in tqdm(range(n_class),desc="Computing mean of each cluster"):
        mean_op[i] = np.mean(catagory_for_traces[i],axis=0)
    # compute the difference of means
    diff_mean = np.zeros(shape=(s_length))
    for i in range(n_class):
        for j in range(i):
            diff_mean += np.abs(mean_op[i] - mean_op[j])
    # select the POI
    poi = np.flip(np.argsort(diff_mean))[:npoi]
    # reduce the dataset
    train_data_red = np.empty((n_training_sample,npoi))
    for i in tqdm(range(n_training_sample),desc="Reducing training dataset"):
        train_data_red[i] = train_data[i][poi]
    test_data_red = np.empty((n_testing_sample,npoi))
    for i in tqdm(range(n_testing_sample),desc="Reducing testing dataset"):
        test_data_red[i] = test_data[i][poi]
    
    return train_data_red, test_data_red

def PCA(train_data, train_targets, test_data, npoi, n_class=2):
    """
    PCA using pricipal component decomposition with numpy linalg library.
    """
    assert train_data.shape[1]==test_data.shape[1], "Training and testing data have different length"
    s_length = train_data.shape[1]
    n_training_sample = train_data.shape[0]
    n_testing_sample = test_data.shape[0]
    # assign traces to clusters based on lables
    catagory_for_traces = [[] for _ in range(n_class)]
    for i in tqdm(range(train_data.shape[0]),desc="Sorting traces per operation"):
        tar = train_targets[i]
        catagory_for_traces[tar].append(train_data[i])
    # compute the mean each cluster
    mean_op = np.empty(shape=(n_class, s_length))
    for i in tqdm(range(n_class),desc="Computing mean of each cluster"):
        mean_op[i] = np.mean(catagory_for_traces[i],axis=0)
    mean_total = np.mean(mean_op,axis=0)
    # Use singular value decomposition to get projective vector
    B = np.zeros(s_length)
    for i in tqdm(range(n_class),desc="Computing projection vector"):
        trace = mean_op[i]-mean_total
        t_trace = np.transpose([trace])
        B = B + trace*t_trace
    B = B/n_class
    U,S,V = np.linalg.svd(B)
    projection = U[:,:npoi]
    # Apply SVD projection to all traces in the datasets
    train_data_red = np.empty((n_training_sample,npoi))
    for i in tqdm(range(n_training_sample),desc="Reducing training dataset"):
        train_data_red[i] = np.dot(train_data[i],projection)
    test_data_red = np.empty((n_testing_sample,npoi))
    for i in tqdm(range(n_testing_sample),desc="Reducing testing dataset"):
        test_data_red[i] = np.dot(test_data[i],projection)

    return train_data_red, test_data_red

def template_building(train_data, train_targets, n_class=16):
    """
    Build templates for each operation.
    """
    assert train_data.shape[1]==test_data.shape[1]
    npoi = train_data.shape[1]
    # assign traces to clusters based on lables
    catagory_for_traces = [[] for _ in range(n_class)]
    for i in tqdm(range(len(train_data)),desc="Sorting traces per operation"):
        tar = train_targets[i]
        catagory_for_traces[tar].append(train_data[i])
    # calculate mean and covariance matrices
    meanMatrix = np.empty(shape=(n_class, npoi))
    covMatrix = np.empty(shape=(n_class, npoi, npoi))
    for i in tqdm(range(n_class),desc="Building templates"):
        meanMatrix[i] = np.mean(catagory_for_traces[i],axis=0)
        covMatrix[i] = np.cov(np.transpose(catagory_for_traces[i]))
    return meanMatrix, covMatrix

def template_predict(test_data, meanMatrix, covMatrix, n_class=16):
    """
    Apply the template matching algorithm to the test data (using pooled template).
    """
    n_traces = test_data.shape[0]
    # Change to the pooled matrix and pre-compute the inverse
    smat_pool = np.sum(covMatrix,axis=0)/n_class
    inv_smat_pool = np.linalg.inv(smat_pool)
    # Compute the template matching
    res = np.empty((n_class,n_traces))
    for i in tqdm(range(n_traces), desc="Computing template matching"):
        for k in range(n_class):
            T_k = test_data[i] - meanMatrix[k]
            res[k,i] = -0.5*(np.dot(np.dot(T_k , inv_smat_pool) , np.transpose(T_k)))
    # Return the ranking of the results
    return np.flip(np.argsort(res,axis=0),axis=0)


if __name__=="__main__":
    parser = argparse.ArgumentParser(description='Template attack in Python')
    parser.add_argument('path', help='Path to file of trs dataset', default="")
    parser.add_argument('-red', help='Type of reduction on dataset samples (choice: pca, dom, None)', default="None", nargs='?')
    parser.add_argument('-npoi', help='Number of points of interrest from reduction method (if no red method is set, npoi is ignored)', default="10", nargs='?')
    parser.add_argument('-leakage', help='Use sbox or ld+sbox', default="sbox", nargs='?')
    parser.add_argument('-subkey', help='Select a subkey if multiple labels are present for each trace', default=0, nargs='?')
    parser.add_argument('-order', help='Order of the output success rate (default:0)', default=0, nargs='?')
    args = parser.parse_args()
    datasetFileName = args.path
    reduction = str(args.red)
    npoi = int(args.npoi)
    leakage = args.leakage
    subkey = int(args.subkey)

    #Load dataset
    dataset = trsfile.trs_open(datasetFileName,'r')
    n_traces = dataset.get_header(trsfile.Header.NUMBER_TRACES)
    s_length = dataset.get_header(trsfile.Header.NUMBER_SAMPLES)
    data =   [dataset[i].samples for i in tqdm(range(n_traces),desc="Reading traces")]
    labels = [dataset[i].parameters['label'].value[subkey] for i in tqdm(range(n_traces),desc="Reading labels")]
    n_class = max(labels)+1
    order = min(int(args.order),n_class-1)

    #Normalize data by removing the mean and scaling to unit variance
    print('Normalizing data..')
    dataset_mean = np.mean(data,axis=0)
    dataset_std = np.std(data,axis=0)
    data = (data-dataset_mean)/dataset_std

    # Split randomly the dataset into training and testing sets
    print('Splitting dataset randomly into training and testing sets..')
    p = np.random.permutation(n_traces)
    train_data = np.array(data)[p[:int(n_traces*0.8)]]
    train_targets = np.array(labels)[p[:int(n_traces*0.8)]]
    test_data = np.array(data)[p[int(n_traces*0.8):]]
    test_targets = np.array(labels)[p[int(n_traces*0.8):]]
    n_test_sample = len(test_data)

    if reduction == "pca":
        ## PCA with numpy
        print('PCA with numpy..')
        train_data, test_data = PCA(train_data, train_targets, test_data, npoi, n_class=n_class)
    elif reduction == "dom":
        ## Diff of means point of interrest selection
        print('Diff of means point of interrest selection..')
        train_data, test_data = diffOfMeansPOI(train_data, train_targets, test_data, npoi, n_class=n_class)
    else:
        print('No reduction..')
    npoi = train_data.shape[1]
    print('shape train_data', train_data.shape, 'shape test data', test_data.shape)
    print('npoi = {}'.format(npoi))

    #Build template
    print('Building template..')
    meanMatrix, covMatrix = template_building(train_data, train_targets, n_class=n_class)

    # Apply the templates to obtain prediction on test_data
    predict = template_predict(test_data, meanMatrix, covMatrix, n_class=n_class)

    #Compute the success rate
    print(test_targets)
    print(predict[:order+1])
    success = np.zeros(n_test_sample)
    for k in range(n_test_sample):
        for o in range(order+1):
            if test_targets[k] in predict[:o+1,k]:
                success[k]=1
    success_rate = np.sum(success)/n_test_sample
    print("Order {} succes rate = {}".format(order, success_rate))

    dataset.close()

