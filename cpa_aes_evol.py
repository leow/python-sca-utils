import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm

HW = np.array([bin(x).count('1') for x in range(0x100)], dtype=np.uint8)

# Rijndael S-box
sbox = np.array([0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67,
    0x2b, 0xfe, 0xd7, 0xab, 0x76, 0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59,
    0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0, 0xb7,
    0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1,
    0x71, 0xd8, 0x31, 0x15, 0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05,
    0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75, 0x09, 0x83,
    0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29,
    0xe3, 0x2f, 0x84, 0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b,
    0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf, 0xd0, 0xef, 0xaa,
    0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c,
    0x9f, 0xa8, 0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc,
    0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2, 0xcd, 0x0c, 0x13, 0xec,
    0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19,
    0x73, 0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee,
    0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb, 0xe0, 0x32, 0x3a, 0x0a, 0x49,
    0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79,
    0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4,
    0xea, 0x65, 0x7a, 0xae, 0x08, 0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6,
    0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a, 0x70,
    0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9,
    0x86, 0xc1, 0x1d, 0x9e, 0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e,
    0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf, 0x8c, 0xa1,
    0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0,
    0x54, 0xbb, 0x16],dtype=np.uint8)

class ConditionalAveragerAesSbox:
    def __init__(self, numValues, traceLength):
        '''Allocate the matrix of averaged traces'''
        self.avtraces = np.zeros((numValues, traceLength))
        self.counters = np.zeros(numValues)
        print('ConditionalAverager: initialized for %d values and trace length %d' % (numValues, traceLength))

    def addTrace(self, data, trace):
        '''Add a single trace with corresponding single chunk of data'''
        if (self.counters[data] == 0):
            self.avtraces[data] = trace
            self.counters[data] += 1
        else:
            self.counters[data] += 1
            self.avtraces[data] = self.avtraces[data] + (trace - self.avtraces[data]) / self.counters[data]

    def getSnapshot(self):
        ''' return a snapshot of the average matrix'''
        avdataSnap = np.flatnonzero(self.counters)   # get an vector of only _observed_ values
        avtracesSnap = self.avtraces[avdataSnap] # remove lines corresponding to non-observed values
        return avdataSnap, avtracesSnap

class CPA_AES(object):
    """
    Class to perform CPA attack on AES SBOX
    """
    def __init__(self, n_traces, tracelen, knownKeyByte, evolutionStep=1, Nskip=20, encrypt=True):
        """Initialize the class

        :param n_traces: Number of traces
        :type n_traces: Int
        :param tracelen: Number of samples per trace
        :type tracelen: Int
        :param knownKeyByte: Known key byte value
        :type knownKeyByte: Int
        :param evolutionStep: Number of traces between each cummulative CPA evaluation, defaults to 1
        :type evolutionStep: int, optional
        :param Nskip: Number of traces to skip before evaluating the first CPA, defaults to 20
        :type Nskip: int, optional
        :param encrypt: Target the encryption/decryption, defaults to True (Unused)
        :type encrypt: bool, optional
        """
        self.knownKeyByte = knownKeyByte
        self.Nskip = Nskip
        self.numSteps = int(np.ceil((N) / np.double(evolutionStep))) + 1
        self.numsamples = tracelen
        self.numtraces = n_traces
        self.numkeys = 256
        self.ranking = None
        self.correctPos = np.zeros((self.numSteps), dtype=np.uint8)
        self.correlation_values = np.empty((self.numkeys,self.numsamples), dtype=np.float32)
        self.correlation_evol = np.empty((self.numkeys,self.numSteps), dtype=np.float32)
        self.CondAver = ConditionalAveragerAesSbox(self.numkeys,self.numsamples)
        self.stepCount = 0
        self.evolutionStep = evolutionStep

    def compute_intermediates(self, i, k):
        """AES encryption intermediate is SBOX[in^k]
        
        :param i: Plaintext
        :type i: Int
        :param k: Key
        :type k: Int
        :return: Intermediate value
        :rtype: Int
        """
        return sbox[i^k]

    def power_model(self, x):
        """Power model (here is Hamming weight)
        
        :param x: Intermediate value
        :type x: Int
        :return: Power model of the intermediate value
        :rtype: Int"""
        return HW[x] ##Hamming weight

    def compute_predictions(self, data):
        """
        Construct a matrix of predictions based on the power model of the intermediate value for each trace/plaintext with all key candidates.

        :param data: Traces
        :type data: Numpy array
        :return: Matrix of predictions
        :rtype: Numpy array
        """
        H = np.zeros((256, len(data)), dtype='uint8') # intermediate variable predictions        
        for k in range(self.numkeys):
            H[k,:] = self.compute_intermediates(data,k)
        HL = np.vectorize(self.power_model)(H).T
        return HL

    def correlationTraces(self, traces, H):
        """
        A fast correlation function on the full matrix of predictions.
        ref: https://github.com/ikizhvatov/efficient-columnwise-correlation
        """
        (n, t) = traces.shape # n traces of t samples
        (n_bis, m) = H.shape  # n predictions for each of m candidates
        assert n_bis == n
        DO = traces - (np.einsum("nt->t", traces, dtype='float64', optimize='optimal') / np.double(n)) # compute O - mean(O)
        DP = H - (np.einsum("nm->m", H, dtype='float64', optimize='optimal') / np.double(n)) # compute P - mean(P)
        numerator = np.einsum("nm,nt->mt", DP, DO, optimize='optimal')
        tmp1 = np.einsum("nm,nm->m", DP, DP, optimize='optimal')
        tmp2 = np.einsum("nt,nt->t", DO, DO, optimize='optimal')
        tmp = np.einsum("m,t->mt", tmp1, tmp2, optimize='optimal')
        denominator = np.sqrt(tmp)
        self.correlation_values = numerator / denominator

    def rank(self):
        """
        Rank the key candidates by taking the the maximum absolute value for all correaltion results.
        """
        self.ranking = np.flip(np.argsort(np.amax(self.correlation_values,1)),0)
        self.correctPos[self.stepCount] = np.argwhere(self.knownKeyByte == self.ranking)[0,0]

    def show_graph(self, fontsize=11):
        """
        Show the graph of:
        - the correlation values for the latest CPA evaluation, 
        - the evolution of the correlation values for all key candidates with increasing number of traces
        - the evolution of therank of the correct key candidate
        """
        fig = plt.figure()
        axCPA = plt.subplot2grid((2, 2), (0, 0), colspan=2)
        axEvol = plt.subplot2grid((2, 2), (1, 0))
        axCorrectPos = plt.subplot2grid((2, 2), (1, 1))

        x = range(self.numsamples)
        for i in range(self.numkeys):
            axCPA.plot(x, self.correlation_values[i], color="blue")
        axCPA.plot(x, self.correlation_values[self.ranking[0]], color="red")
        axCPA.set_ylabel('Correlation', fontsize=fontsize)
        axCPA.set_xlabel('Time sample', fontsize=fontsize)

        x = np.arange(0, self.stepCount)*self.evolutionStep
        axCorrectPos.plot(x, self.correctPos)
        axCorrectPos.set_ylabel('Correct key candidate rank', fontsize=fontsize)
        axCorrectPos.set_xlabel('Number of traces', fontsize=fontsize)

        for i in range(self.correlation_evol.shape[0]):
            axEvol.plot(x, self.correlation_evol[i], color='gray', linewidth=0.4)
        axEvol.plot(x, self.correlation_evol[self.knownKeyByte], color='red')
        axEvol.set_ylabel('Correlation', fontsize=fontsize)
        axEvol.set_xlabel('Number of traces', fontsize=fontsize)
        plt.tight_layout()
        plt.show()

    def run(self, data, traces):
        """
        Run the CPA attack on the traces and data provided.
        
        :param data: Traces
        :type data: Numpy array
        :param traces: Data
        :type traces: Numpy array
        """
        for i in range(self.Nskip-1):
            self.CondAver.addTrace(data[i], traces[i])
        for i in tqdm(range(self.Nskip-1, self.numtraces+self.Nskip)):
            self.CondAver.addTrace(data[i], traces[i])

            if (((i + 1) % self.evolutionStep == 0) or ((i + 1) == self.numtraces+self.Nskip)):
                (avdata, avtraces) = self.CondAver.getSnapshot()
                H = self.compute_predictions(avdata)
                self.correlationTraces(avtraces, H)
                self.correlation_evol[:,self.stepCount] = np.max(np.abs(self.correlation_values), axis=1)
                self.rank()
                self.stepCount += 1
        self.show_graph()


if __name__ == "__main__":
    tracesetFilename = "traces/swaes_atmega_power.npz"
    knownKey = bytes.fromhex("2B7E151628AED2A6ABF7158809CF4F3C") # the correct key
    npzfile = np.load(tracesetFilename)
    N        = 1000 # number of traces to attack (less or equal to the amount of traces in the file)
    Nskip    = 5 #number of traces to skip to avoid warning in correlation for division by 0
    SboxNum  = 0   # S-box to attack, counting from 0
    data = npzfile['data'][:N+Nskip,SboxNum] # selecting only the required plaintext byte
    traces = npzfile['traces'][:N+Nskip, 800:1500]
    evolutionStep = 5

    cpa_aes = CPA_AES(N, traces.shape[1], knownKey[SboxNum], evolutionStep=evolutionStep, Nskip=Nskip)
    cpa_aes.run(data, traces)
