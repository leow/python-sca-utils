import sys
import numpy as np
import trsfile
import matplotlib.pyplot as plt
from tqdm import tqdm
import argparse

class TTEST(object):
    """T-test class to compute t-test"""
    def __init__(self, Ns):
        self.Ns = Ns
        self.Nt = np.zeros(2, dtype=np.int32)
        self.Nt_total = 0
        self.t_sum = np.zeros((2, Ns), dtype=np.float64)
        self.t_sum2 = np.zeros((2, Ns), dtype=np.float64)
        self.t_var = np.empty((2,Ns), dtype=np.float64)
        self.tstat = np.zeros((Ns), dtype=np.float64)

    def ttest_trsfile(self, traceset, n_traces=-1, flag="ttest", disable_tqdm=False):
        """Compute t-test on trsfile dataset. If this function is called multiple times, the t-test is updated with the new traces.

        :param traceset: trsfile dataset
        :type traceset: trsfile.Dataset
        :param disable_tqdm: Print a loading bar to visualize progress, defaults to False
        :type disable_tqdm: bool, optional
        :return: t-test values
        :rtype: np.array
        """
        #check if trsfile is legacy
        try:
            trs_version = traceset.get_header(trsfile.Header.TRS_VERSION)
        except:
            trs_version = 1
        param_key = "LEGACY_DATA" if trs_version == 1 else flag
        #Var[X] = E[X**2] - E[X]**2
        for trace in tqdm(traceset[self.Nt_total:n_traces], desc="Trace compute", disable=disable_tqdm):
            t = trace.samples
            self.t_sum[trace.parameters[param_key].value[0]] += t.astype(np.float64)
            self.t_sum2[trace.parameters[param_key].value[0]] += t*t.astype(np.float64)
            self.Nt[trace.parameters[param_key].value[0]] += 1
        self.Nt_total = self.Nt[0] + self.Nt[1]

        for i in range(2):
            self.t_var[i] = self.t_sum2[i]/self.Nt[i] - (self.t_sum[i]/self.Nt[i])**2
        # Tstat = (E[X] - E[Y])/(sqtr(Var[X]/Nx + Var[Y]/Ny))
        self.tstat = (self.t_sum[0]/self.Nt[0] - self.t_sum[1]/self.Nt[1])/np.sqrt((self.t_var[0]/self.Nt[0]) + (self.t_var[1]/self.Nt[1]))
        return self.tstat


if __name__=="__main__":
    parser = argparse.ArgumentParser(description='T-Test function in Python')
    parser.add_argument('path', help='Path to file of trs dataset, trs file must contain traces for ttest with parameter `ttest`(or flag defined with -t) or as first byte of data', default="", nargs='?')
    parser.add_argument('-t', help='Parameter flag of ttest value', default="ttest", nargs='?')
    parser.add_argument('-n', help='Number of traces to analyze', default=-1, nargs='?')
    args = parser.parse_args()
    ### Global variables
    datasetFileName = str(args.path)
    flag = str(args.t)
    n_traces = int(args.n)

    traceset = trsfile.trs_open(datasetFileName,'r')
    assert flag in traceset[0].parameters, "Dataset does not contain ttest parameter, available parameters are: {}".format(list(traceset[0].parameters)) 
    ttest_object = TTEST(traceset.get_header(trsfile.Header.NUMBER_SAMPLES))
    tstat = abs(ttest_object.ttest_trsfile(traceset, n_traces=n_traces, flag=flag))

    #Plot tstat
    fig, ax1 = plt.subplots()
    ax1.set_ylabel("t-values")
    plot_1 = ax1.plot(tstat, label='t-stat')
    ax1.plot(4.5*np.ones(len(tstat)), color='red')
    ax2 = ax1.twinx()
    scale_y = traceset.get_header(trsfile.Header.SCALE_Y) if trsfile.Header.SCALE_Y in traceset.get_headers() else 1
    plot_2 = ax2.plot(scale_y*traceset[0].samples, color='gray', alpha=0.5, label='power trace')
    ax2.set_ylabel("V")
    lns = plot_1 + plot_2
    labels = [l.get_label() for l in lns]
    plt.legend(lns, labels, loc=0)
    plt.title("T-Test")
    plt.show()

    traceset.close()