# Python SCA utils
A collection of Python scripts for side-channel analysis.

These scripts use trsfile traceset format.
Trsfile tracesets are formed with samples (i.e., the side-channel traces) and parameters, a dictionnary-like object used to store any information like: plaintext, key, ttest, etc..

## TTEST.py
Simple T-test between two sets. 

Construct a dataset with labels 0,1 as a `ttest` parameter and side-channel traces.

$t = \frac{E[X] - E[Y]}{\sqrt{\frac{Var[X]}{N_x} + \frac{Var[Y]}{N_y}}}$

<img src="images/ttest.png" alt="ttest.png" width="40%"/>

## SNR.py
Fast Signal-to-Noise Ratio function. To inspect the samples with more signals and overall noise.

$SNR = \frac{VAR(signal)}{VAR(noise)}$

Construct a dataset with labels 0,..,n_class as a `label` corresponding to an intermediate value and side-channel traces.

Example on Dpav4 dataset

<img src="images/snr.png" alt="snr.png" width="40%"/>

## SOSD.py
Sum of squared difference. Common method for statistics of a dataset.

$\sum_{j,l=1}^K (m_j - m_l)^2, l>=j$

Construct a dataset with labels 0,..,n_class as a `label` corresponding to an intermediate value and side-channel traces.

Example on Dpav4 dataset

<img src="images/sosd.png" alt="snr.png" width="40%"/>

## SOST.py
Sum of pairwise t-differences. Close to a SNR, but designed on SOSD instead.

$\sum_{i,j=1}^K \left(\frac{m_i - m_j}{\sqrt{\frac{\sigma_i^2}{n_i} + \frac{\sigma_j^2}{n_j}}}\right)^2, i>=j$

Construct a dataset with labels 0,..,n_class as a `label` corresponding to an intermediate value and side-channel traces.

Example on Dpav4 dataset

<img src="images/sost.png" alt="snr.png" width="40%"/>

## TA.py
Template attack with pooled templates.

Construct a dataset with labels 0,..,n_class as a `label` corresponding to an intermediate value and side-channel traces.

## cpa_aes.py
Example of a CPA attack on AES

Output:

<img src="images/cpa_aes.png" alt="cpa_aes.png" width="40%"/>

## cpa_aes_evol.py
A more advanced example of the same CPA attack on AES

Output:

<img src="images/cpa_aes_evol.png" alt="cpa_aes_evol.png" width="40%"/>
