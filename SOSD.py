import numpy as np
import argparse
import trsfile
import time
import matplotlib.pyplot as plt
from tqdm import tqdm

#Sum of squared difference (SOSD): sumj,l=1^K(m_j - m_l)^2, l>=j
#ref: "Templates vs. Stochastic Methods" Gierlichs, Benedikt and Lemke-Rust, Kerstin and Paar, Christof - CHES 2006
def SOSD(traces, labels):
    # Get the number of traces and number of samples per trace
    Nt, Ns = (len(traces), len(traces[0]))
    classes = np.unique(labels)
    Nc = len(classes)
    # Number of observed traces in each intermediate value
    ns = np.zeros((Nc),dtype=np.uint32)
    # Sum for each class
    sum1 = np.zeros((Nc,Ns),dtype=np.float64)
    # Mean of each class
    means = np.zeros((Nc,Ns),dtype=np.float64)
    # Sum of squared difference (SOSD)
    sosd = np.zeros((Ns),dtype=np.float64)

    # Compute sum for all traces given labels
    for i in range(Nt):
        c_i = np.where(classes==labels[i])
        ns[c_i] += 1
        t = traces[i]
        sum1[c_i] += t

    # Compute mean and variance on every class
    for c in range(Nc):
        means[c,:] = (sum1[c] / ns[c]) # standard mean
    for j in range(Nc):
        for l in range(j,Nc):
            sosd += (means[j] - means[l])**2
    return sosd

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Sum of squared difference (SOSD) in Python')
    parser.add_argument('path', help='Path to file of trs dataset', default="", nargs='?')
    parser.add_argument('-hw', help='Transform labels into Hamming Weight model', default=False, nargs='?')
    parser.add_argument('-n', help='Number of traces to analyze', default=-1, nargs='?')
    args = parser.parse_args()
    datasetFileName = args.path
    HW = args.hw
    n_traces = int(args.n)
    
    #Load dataset
    dataset = trsfile.trs_open(datasetFileName,'r')
    n_samples = dataset.get_header(trsfile.Header.NUMBER_SAMPLES)
    traces = [trace.samples for trace in tqdm(dataset[:n_traces],desc="Reading traces")]
    labels = [trace.parameters['label'].value[0] for trace in tqdm(dataset[:n_traces],desc="Reading labels")]
    if HW:
        labels = [bin(x).count('1') for x in labels]
    n_class = max(labels)+1
    print("Number of operations: ",n_class)
    
    #Compute SOSD
    a = time.time()
    sosd = SOSD(traces, labels)
    print("Time: ",time.time()-a)
    #Plot SOSD
    plt.plot(sosd)
    plt.title("Sum of squared difference (SOSD)")
    plt.xlabel("Sample")
    plt.ylabel("SOSD")
    plt.show()

    dataset.close()